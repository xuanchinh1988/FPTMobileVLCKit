Pod::Spec.new do |s|
  s.name         = "FPTMobileVLCKit"
  s.version      = "1.0.1"
  s.summary      = "MobileVLCKit is an Objective-C wrapper for libvlc's external interface on iOS."
  s.homepage     = 'https://gitlab.com/bauloc/FPTMobileVLCKit'
  s.license      = {
    :type => 'LGPL v2.1', :file => 'LICENSE'
  }
  s.authors      = 'Pierre d\'Herbemont', { 'Felix Paul Kühne' => 'fkuehne@videolan.org' }
  s.source       = { :git => "https://gitlab.com/bauloc/FPTMobileVLCKit.git", :tag => s.version }
  s.platform     = :ios
  s.public_header_files = "FPTMobileVLCKit/*.framework/Headers/*.h"
  s.source_files = "FPTMobileVLCKit/*.framework/Headers/*.h"
  s.vendored_frameworks = "FPTMobileVLCKit/DynamicMobileVLCKit.framework"
  
  # vlc kit dependencies
  s.frameworks = "QuartzCore", "CoreText", "AVFoundation", "Security", "CFNetwork", "AudioToolbox", "OpenGLES", "CoreGraphics", "VideoToolbox", "CoreMedia"
  s.libraries = "c++", "xml2", "z", "bz2", "iconv"
  s.xcconfig = {
    'CLANG_CXX_LANGUAGE_STANDARD' => "c++11",
    'CLANG_CXX_LIBRARY' => "libc++"
  }

  s.ios.deployment_target  = '10.0'
end