# FPTMobileVLCKit

[![CI Status](https://img.shields.io/travis/bauloc/FPTMobileVLCKit.svg?style=flat)](https://travis-ci.org/bauloc/FPTMobileVLCKit)
[![Version](https://img.shields.io/cocoapods/v/FPTMobileVLCKit.svg?style=flat)](https://cocoapods.org/pods/FPTMobileVLCKit)
[![License](https://img.shields.io/cocoapods/l/FPTMobileVLCKit.svg?style=flat)](https://cocoapods.org/pods/FPTMobileVLCKit)
[![Platform](https://img.shields.io/cocoapods/p/FPTMobileVLCKit.svg?style=flat)](https://cocoapods.org/pods/FPTMobileVLCKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
iOS 10+ <br />
Xcode 11+ <br />
Support iOS: Simulator(i386 x86_64) Device(armv7 arm64) <br />

## Installation

FPTMobileVLCKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FPTMobileVLCKit'
```

## Author

bauloc, loc.plsoft@gmail.com

## Document

Build on VLCKIT 3.3.13 <br />
https://wiki.videolan.org/VLCKit#Building_the_framework_for_iOS <br />
https://medium.com/@heitorburger/static-libraries-frameworks-and-bitcode-6d8f784478a9 <br />
https://stackoverflow.com/questions/56957632/could-not-find-module-for-target-x86-64-apple-ios-simulator <br />

## Sample Player

https://gitlab.com/bauloc-fpt/VLCMobileKitExample

## License

FPTMobileVLCKit is available under the MIT license. See the LICENSE file for more info.
